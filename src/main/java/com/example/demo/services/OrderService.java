package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.example.demo.models.Address;
import com.example.demo.models.Order;


@Service
public class OrderService {
	final static Logger logger = LoggerFactory.getLogger(OrderService.class);
	public int create() {
		return 1;
	}
	
	public Order getOrder() {
		List<Address> addresses = new ArrayList<>();
		addresses.add(new Address(6127374, "Rawa belong"));
		addresses.add(new Address(9812384, "Palmerah Barat"));
		Order order = new Order();
		order.setId(1);
		order.setItem("Laptop");
		order.setPrice(23000);
		order.setAddresses(addresses);
		return order;
		
	}
}
