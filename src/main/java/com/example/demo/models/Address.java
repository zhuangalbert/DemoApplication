package com.example.demo.models;

public class Address {
	int pin;
	String street;

	public Address(int pin, String street) {
		super();
		this.pin = pin;
		this.street = street;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
}
