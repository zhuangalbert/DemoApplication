package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Order;
import com.example.demo.services.OrderService;

@RestController
@RequestMapping("order")
public class OrderController {
	final static Logger logger = LoggerFactory.getLogger(OrderController.class);
	@Autowired
	OrderService orderService;
	@PostMapping()
	public int createOrder(String order) {
		return orderService.create();
	}
	
	@GetMapping()
	public List<Order> submitOrder() {
		List<Order> orders = new ArrayList<>();
		orders.add(orderService.getOrder());
		return orders;
	}
}
