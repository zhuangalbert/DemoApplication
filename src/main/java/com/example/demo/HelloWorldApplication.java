package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldApplication {

	public static void main(String[] args) {
		Logger logger = LoggerFactory.getLogger(HelloWorldApplication.class);
		SpringApplication.run(HelloWorldApplication.class, args); //spring container
		System.out.println("Spring Container Created");
		logger.info("Spring Container Running");
		logger.trace("TRACE Logging");
	}

}
